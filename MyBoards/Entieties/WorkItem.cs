﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyBoards.Entieties
{
    public class Epic : WorkItem
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }

    public class Issue : WorkItem
    {
        public decimal Efford { get; set; }
    }

    public class Task : WorkItem
    {
        public string Activity { get; set; }
        public decimal RemaningWork { get; set; }
    }
    public abstract class WorkItem
    {
        // [Required] setting elem to required
        // [MaxLength(200)] Changing length of NVarChar for activity
        // [Column(TypeName = "varchar(200)")] changing type of column in DB
        // [Column("Iteration_Path")] changing name of column in DB

        public int Id { get; set; }
        public WorkItemState State { get; set; }
        public int StateId { get; set; }
        public string Area { get; set; }      
        public string IterationPath { get; set; }
        public int Priority { get; set; }

        // Configuration one-to-many relationship with comment
        public List<Comment> Comments { get; set; } = new List<Comment>();

        // Configuration one-to-many relationship with user
        public User Author { get; set; }
        public Guid AuthorId { get; set; }

        public List<Tag> Tags { get; set; }
    }
}

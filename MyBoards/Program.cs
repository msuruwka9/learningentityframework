using Microsoft.AspNetCore.Http.Json;
using Microsoft.EntityFrameworkCore;
using MyBoards.Entieties;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.Configure<JsonOptions>(options =>
{
    options.SerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
});

builder.Services.AddDbContext<MyBoardsContext>(
        option => option.UseSqlServer(builder.Configuration.GetConnectionString("MyBoardsConnectionString"))
    );

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

using var scope = app.Services.CreateScope();
var dbContext = scope.ServiceProvider.GetService<MyBoardsContext>();

var pendingMigrations = dbContext.Database.GetPendingMigrations();
if (pendingMigrations.Any())
{
    dbContext.Database.Migrate();
}

var users = dbContext.Users.ToList();
if (!users.Any())
{
    var user1 = new User()
    {
        Email = "user1@test.com",
        FullName = "User One",
        Address = new Address()
        {
            City = "Warszawa",
            Street = "Szeroka"
        }
    };

    var user2 = new User()
    {
        Email = "user2@test.com",
        FullName = "User Twp",
        Address = new Address()
        {
            City = "Krak�w",
            Street = "D�uga"
        }
    };

    dbContext.Users.AddRange(user1, user2);

    dbContext.SaveChanges();
}

app.MapGet("data", async (MyBoardsContext db) =>
{
    //var user = await db.Users
    //.Include(u => u.Comments).ThenInclude(c => c.WorkItem)
    //.Include(u => u.Address)
    //.FirstAsync(u => u.Id == Guid.Parse("68366DBE-0809-490F-CC1D-08DA10AB0E61"));

    //var userComments = await db.Comments.Where(c => c.AuthorId == user.Id).ToListAsync();

    // optimized deletion of user, only If I know Data
    /*var workItem = new Epic()
    {
        Id = 2
    };

    var entry = db.Attach(workItem);
    entry.State = EntityState.Deleted;

    db.SaveChanges();*/

    var states = db.WorkItemStates
                    .AsNoTracking()
                    .ToList();

    var entries1 = db.ChangeTracker.Entries();   
    //this line checks values inside ChangeTracker

    return states;
});

app.MapPost("update", async (MyBoardsContext db) =>
{
    var epic = await db.Epics.FirstAsync(epic => epic.Id == 1);

    var rejectedState = await db.WorkItemStates.FirstAsync(a => a.Value == "Rejected");

    epic.State = rejectedState;

    await db.SaveChangesAsync();

    return epic;
});

app.MapPost("create", async (MyBoardsContext db) =>
{
    var address = new Address()
    {
        Id = Guid.Parse("b323dd7c-776a-4cf6-a92a-12df154b4a2a"),
        City = "Krak�w",
        Country = "Poland",
        Street = "D�uga"
    };

    var user = new User()
    {
        Email = "user5@test.com",
        FullName = "Test User5",
        Address = address,
    };

    db.Users.Add(user); 
    // bc User has relation with address, I don't have to explicitly add address to db

    await db.SaveChangesAsync();

    return user;
});

app.MapDelete("delete", async (MyBoardsContext db) =>
{
    var user = await db.Users
        .Include(u => u.Comments) //need to include this for clientcascade delete
        .FirstAsync(u => u.Id == Guid.Parse("4EBB526D-2196-41E1-CBDA-08DA10AB0E61"));
    // where there is not cascade delete I have to delete comments associated with users first
    //var userComments = db.Comments.Where(c => c.AuthorId == user.Id).ToList();
    //db.RemoveRange(userComments);
    //await db.SaveChangesAsync();

    db.Users.Remove(user);

    await db.SaveChangesAsync();
});

app.Run();
﻿namespace MyBoards.Entieties
{
    public class User
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }

        // Creation of relation one-to-one with Address
        public Address Address { get; set; }

        // Configuration one-to-many relationship with WorkItems
        public List<WorkItem> WorkItems { get; set; } = new List<WorkItem>();

        public List<Comment> Comments { get; set; } = new List<Comment>();
    }
}

﻿namespace MyBoards.Entieties
{
    public class Comment
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public User Author { get; set; }
        public Guid AuthorId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        // Configuration one-to-many relationship
        public WorkItem WorkItem { get; set; }
        public int WorkItemId { get; set; } 
        // if there is not WorkItemId EF will create shadow FK
        // it is better to have explicit FK
    }
}
